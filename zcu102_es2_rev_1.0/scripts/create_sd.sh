#!/bin/bash

sudo mkfs.vfat -F 32 -n boot /dev/sdd1
sudo mkfs.ext4 -L root /dev/sdd2

sudo mount /dev/sdd1 /mnt/boot
sudo cp BOOT.BIN Image system.dtb /mnt/boot/
sudo umount /mnt/boot

sudo mount /dev/sdd2 /mnt/rfs
cd /mnt/rfs
sudo cpio -iv --no-absolute-filenames < /storage/CAD/bl_test/bl1/xilinx-zcu102-zu9-es2-rev1.0-2016.4/images/linux/rootfs.cpio
cd -
sudo umount /mnt/rfs

